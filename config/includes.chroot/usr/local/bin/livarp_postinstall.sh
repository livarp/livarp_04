#!/bin/bash
############################################
## script de post-installation livarp_0.4 ##
##----------------------------------------##

# enable colors
source ~/.bashrc
clear

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " you're not root..."
    echo " this script needs admin privileges"
    echo "   please launch this script as root."
    sleep 4s && exit 0
fi

# run only once
# ----------------------------------------------------------------------
touch /usr/share/livarp/livarpfirstlog
echo "file auto-generated by livarp_post-install script. don't remove." > /usr/share/livarp/livarpfirstlog

# set user
# ----------------------------------------------------------------------
HUMAN=`w -h | tail -n1 | awk '{print $1}'`

# allow user to sudo halt/reboot without password
# ----------------------------------------------------------------------
if [ "`cat /etc/sudoers | grep shutdown`" = "" ]; then
    echo "$HUMAN    ALL=(ALL:ALL) NOPASSWD: /sbin/shutdown, /sbin/reboot" >> /etc/sudoers
fi

# copy Debian Wheezy main sources.list
# ----------------------------------------------------------------------
cp /usr/share/livarp/sources.list.free /etc/apt/sources.list

# fix network-manager
# ----------------------------------------------------------------------
sed --in-place=.bak 's/managed=false/managed=true/' /etc/NetworkManager/NetworkManager.conf

# message
# ----------------------------------------------------------------------
clear
echo ""
echo -e "${blue} #############################################################"
echo -e "${blue} #         welcome on livarp_0.4 post-install script,        #"
echo -e "${blue} ##---------------------------------------------------------##"
echo ""
echo -e "${cyan}  this script finish the livarp_0.4 install process:$NC"
echo "  - allow $HUMAN to shutdown or reboot without password ...done"
echo "  - set free sources.list ...done"
echo "  - fix network-manager ...done but effective on reboot"
echo ""
echo -e "${cyan} by visiting your ~/bin/clickmes/ directory with rox-filer,"
echo -e "${cyan} you can :$NC"
echo "  - install the 100% free linux-libre kernel,"
echo "  - install software-packs,"
echo "  - remove uneeded sessions one-by-one,"
echo "  - install non-free stuff (if you really need to...)"
echo ""
echo ""
echo -e "${cyan}             have a nice trip with livarp$NC"
echo " feel free to mail me if you have suggestions or issues"
echo "                in Debian we trust.."
echo ""
echo -n " hit Enter to leave"
read anykey
exit 0
